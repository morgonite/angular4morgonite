import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';



import { AppComponent } from './app.component';
import{ContactUsComponent} from './Morgonite/contact-us.component';
import { MainPageComponent } from './morgonite/main-page/main-page.component';


@NgModule({
  declarations: [
    AppComponent,
    ContactUsComponent,
    MainPageComponent
  ],
  imports: [
    BrowserModule
    ,NgbModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
